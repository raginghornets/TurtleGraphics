import turtlegraphics.Turtle;
import turtlegraphics.TurtleException;

public class Tortoise extends Turtle
{
	int zeroX = 599;
	int zeroY = 599;
	
	public void DrawFibSquares(int n) throws TurtleException
	{
		for(int i=1;i<=n;i++)
		{
			DrawPolygon(4, fibSeries(i));
			turnLeft(90);
			int distance = (fibSeries(i+1) - fibSeries(i))/2;
			if(distance != 0)
			{
				move(distance);
			}
			turnRight(90);
		}
	}

	public int fibSeries(int n)
	{
		if(n == 0)
		{
			return 0;
		}
		else if(n == 1)
		{
			return 1;
		}
		else
		{
			int x = fibSeries(n-1) + fibSeries(n-2);
			//System.out.println("fibSeries(" + n + "-1) + " + "fibSeries(" + n + "-2)" + ": " + fibSeries(n-1) + " + " +  fibSeries(n-2) + " = "+ g);
			return x;
		}
	}
	
	public String penState()
	{
		if(!isPenDown())
		{
			return "Up";
		}
		else
		{
			return "Down";
		}
	}
	
	public String toString()
	{
		// location, direction, penState
		String s;
		s = "Class: " + getClass() + "\n";
		s += " Column: " + atColumn() + "\n";
		s += " Row: " + atRow() + "\n";
		s += " Direction: " + direction() + "\n";
		s += " Pen State: "  + penState() + "\n";
		return s;
	}
	
	/**
	 * Turns the Tortoise to the left given the number of degrees
	 * @param degrees
	 * @throws TurtleException
	 */
	public void turnLeft(int degrees) throws TurtleException
	{
		if(degrees > 180 || degrees < 0)
		{
			throw new IllegalArgumentException("The input must be between 0 and 180");
		}
		else
		{
			//System.out.println("Turn Left:\n -Turning Right 180");
			turnRight(180);
			//System.out.println(" -Turning Right " + (180-degrees));
			turnRight(180-degrees);
		}
	}
	
	public void DrawPolygon(int numSides, int sideLength) throws TurtleException
	{
		for(int i=0;i<numSides;i++)
		{
			move(sideLength);
			turnRight(360/numSides);
		}
	}
	
	/**
	 * Draws a triangle
	 * @param sideLength Integer which determines the size of the triangle
	 * @throws TurtleException
	 */
	public void DrawTriangle(int sideLength) throws TurtleException
	{
		turnRight(30);
		for(int i=0;i<3;i++)
		{
			move(sideLength);
			turnRight(120);
		}
	}
	
	/**
	 * Draws 6 hexagons, creating the illusion of 3 cubes
	 * @param sideLength Integer which determines the size of the illusion
	 * @throws TurtleException
	 */
	public void DrawThreeCubes(int sideLength) throws TurtleException
	{
		for(int i=0;i<6;i++)
		{
			DrawPolygon(6, sideLength/2);
			turnRight(60);
		}
	}
	
	/**
	 * Sets the origin of the Tortoise to (x,y)
	 * @param x The X coordinate of the new origin
	 * @param y The Y coordinate of the new origin
	 * @throws TurtleException
	 */
	public void setOriginAt(int x, int y) throws TurtleException
	{
		jumpTo(x, y);
		zeroX += x;
		zeroY += y;
		System.out.print("Origin: ");
		printPoint(zeroX, zeroY);
	}
	
	/**
	 * Sets position of the Tortoise to (x,y)
	 * @param x The X position to jump to
	 * @param y The Y position to jump to
	 * @throws TurtleException
	 */
	public void jumpTo(int x, int y) throws TurtleException
	{
		y *= -1;
		x += zeroX;
		y += zeroY;
		System.out.println("jumpTo(): " + printPoint(x, y));
		if(isPenDown())
		{
			penUp();
		}
		int distanceX = Math.abs(x-atColumn());
		int distanceY = Math.abs(y-atRow());
		System.out.println("jumpTo(): distanceX: " + x + " - " + atColumn() + " = " + distanceX);
		System.out.println("jumpTo(): distanceY: " + y + " - " + atRow() + " = " + distanceY);
		if(x == zeroX)
		{
			if(y <= zeroY)
			{
				turnRight(180);
			}
		}
		else if(x > zeroX)
		{
			if(distanceX != 0)
			{
				turnRightMove(90-direction(), distanceX);
			}
			else
			{
				turnRight(90-direction());
			}
			if(y > zeroY)
			{
				turnLeft(90);
			}
			else if(y < zeroY)
			{
				turnRight(90);
			}
		}
		else if(x < zeroX)
		{
			if(distanceX != 0)
			{
				turnLeftMove(90-direction(), distanceX);
			}
			else
			{
				turnRight(90-direction());
			}
			if(y > zeroY)
			{
				turnRight(90);
			}
			else if(y < zeroY)
			{
				turnLeft(90);
			}
		}
		if(distanceY != 0)
		{
			move(distanceY);
		}
		if(!isPenDown())
		{
			penDown();
		}
		printPosition();	
		resetDirection();
	}
	
	/**
	 * Move Tortoise to (x,y)
	 * @param x
	 * @param y
	 * @throws TurtleException
	 */
	public void moveTo(int x, int y) throws TurtleException
	{
		boolean negativeX = x < 0;
		int x1 = x;
		int y1 = y;
		x = y1;
		y = x1;
		x += zeroX;
		y += zeroY;
		double angle = Math.abs(Math.atan2(y - atRow(), x - atColumn()));
		angle = angle * (180/Math.PI);
		if(negativeX)
		{
			angle = 360 - angle;
		}
		setDirection((int) angle);
		System.out.println("Angle: " + angle);
		double distance = Math.sqrt( ( (y-atRow())*(y-atRow()) ) + ( (x-atColumn())*(x-atColumn()) ));
		System.out.println("Distance from "+ printPoint(atColumn(), atRow()) + " to " + printPoint(x,y) + ": " + (int) distance);
		if(!isPenDown())
		{
			penDown();
		}
		move((int) distance);
		printPosition();
	}
	
	/**
	 * Draws X axis and Y axis
	 * @throws TurtleException
	 */
	public void drawAxes() throws TurtleException
	{
		drawAxisY();
		turnRight(90);
		drawAxisX();
		System.out.println("drawAxes(): Drew axes");
	}
	
	/**
	 * Draws a line based on the maximum number of rows of the window
	 * @throws TurtleException
	 */
	public void drawAxisY() throws TurtleException
	{
		moveTurnRight(MAX_ROW - zeroY - 1, 180);
		moveTurnRight(MAX_ROW, 180);
		move(MAX_ROW - (MAX_ROW - zeroY) + 1); printPosition();
	}
	
	/**
	 * Draws a line based on the maximum number of columns of the window
	 * @throws TurtleException
	 */
	public void drawAxisX() throws TurtleException
	{
		moveTurnRight(MAX_COLUMN - zeroX, 180);
		moveTurnRight(MAX_COLUMN, 180);
		moveTurnLeft(MAX_COLUMN - (MAX_COLUMN - zeroX), 90);
	}
	
	public void moveTurnRight(int distance, int direction) throws TurtleException
	{
		move(distance);
		//printPosition();
		turnRight(direction);
	}
	
	public void moveTurnLeft(int distance, int direction) throws TurtleException
	{
		move(distance);
		//printPosition();
		turnLeft(direction);
	}
	
	public void turnRightMove(int direction, int distance) throws TurtleException
	{
		turnRight(direction);
		move(distance);
		printPosition();
	}
	
	public void turnLeftMove(int direction, int distance) throws TurtleException
	{
		turnLeft(direction);
		move(distance);
		printPosition();
	}
	
	/**
	 * Prints the current position of the Tortoise
	 */
	public void printPosition()
	{
		System.out.println("printPosition(): Window coordinates: " + printPoint(atColumn(), atRow()));
		//System.out.println("New coordinates: " + printPoint(atColumn() - zeroX, atRow() - zeroY));
	}
	
	/**
	 * Sets Tortoise direction to 0
	 * @throws TurtleException
	 */
	public void resetDirection() throws TurtleException
	{
		if(direction() == 0)
		{
			System.out.println("resetDirection(): Already 0");
		}
		else if(direction() > 0 && direction() <= 180)
		{
			turnLeft(direction());
		}
		else if(direction() > 180 && direction() < 360)
		{
			turnLeft(direction()-180);
			turnLeft(direction());
		}
		System.out.println("resetDirection(): Reset direction: " + direction());
	}
	
	/**
	 * Set the direction of the Tortoise
	 * @param degrees
	 * @throws TurtleException
	 */
	public void setDirection(int degrees) throws TurtleException
	{
		resetDirection();
		if(degrees >= 0 && degrees <= 180)
		{
			turnRight(degrees);
		}
		else if(degrees > 180 && degrees < 360)
		{
			turnLeft(180-(degrees-180));
		}
		System.out.println("setDirection(): New direction: " + direction());
	}
	
	/**
	 * Prints two integers into point format
	 * @param x Integer 1
	 * @param y Integer 2
	 */
	public String printPoint(int x, int y)
	{
		return ("("+x+","+y+")");
	}
	
	public void drawAsterisk(int numSpokes, int spokeLength) throws TurtleException
	{
		for(int i=0;i<numSpokes;i++)
		{
			move(spokeLength);
			turnRight(180);
			move(spokeLength);
			turnRight(180);
			turnRight(360/numSpokes);
		}
	}
}
