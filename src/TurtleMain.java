import turtlegraphics.TurtleException;

public class TurtleMain 
{
	public static Tortoise tortoise;
	public static void main(String[] args) throws TurtleException 
	{
		tortoise = new Tortoise();
		tortoise.DrawFibSquares(14);
	}
}
